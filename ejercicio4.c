/* PROGRAMA PARA LEER EL NOMBRE DE UNA PERSONA Y SU EDAD*/

#include<stdio.h>

int main(){
    int edad;
    char nombre[10];        // Este tipo de str siempre tiene que estar inicializado

    printf("Ingrese su nombre: ");
    scanf("%s",nombre);

    printf("Edad: ");
    scanf("%i",&edad);

    printf("Bienvenido %s su edad es %i \n",nombre,edad);

    return 0;
}