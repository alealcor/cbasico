#include<stdio.h>

int main(){

    int n1, n2, n3, suma;
    float resultado;

    printf("Ingrese 3 numeros separados de una coma (n1,n2,n3): ");
    scanf("%i,%i,%i",&n1,&n2,&n3);
    suma = n1+n2+n3;
    resultado = ((float)suma) / n1;

    printf("El resultado es: %f \n", resultado);

    return 0;
}