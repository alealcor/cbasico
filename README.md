# ⚠️ OE ⚠️

### Instrucciones
* _Ahi esta el pdf._
* _Evita hacer algun commit en el repositorio, si deseas editar clonalo, o si tienes cuenta de git haz un fork._
* _Me da pereza buscar la configuracion._
* _Los archivos.c son los importantes... los que no tienen extension son los ejecutables de cada problema._

_Pilas, Lion out 🎤👎_

### Recomendaciones

Para compilar el archivo.c puedes hacerlo con el siguiente comando en el **shell de Linux**

```
gcc -Wall archivo.c -o nombre_ejecutable
```
Y para ejecutar:
```
./nombre_ejecutable
```
Esta de mas decir que **archivo.c** y **nombre_ejecutable** dependeran de: el nombre de tu archivo que quieres compilar y el que le quieres dar al ejecutable. Solo digo.

La bandera **-Wall** muestra las advertencias que tiene tu codigo y donde deberias corregirlo.


[Alex_Cordova](https://gitlab.com/alealcor)